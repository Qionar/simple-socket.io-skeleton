require('dotenv').config()

const express = require('express')
const { createServer } = require('http')
const { Server } = require('socket.io')


const runServer = (beforeCallback, socketsCallback) => {
    const app = express()
    app.use(express.json())

    beforeCallback(app)

    const server = createServer(app)

    const io = new Server(server, {})

    io.on("connection", (socket) => {
        socketsCallback(socket, io)
    })

    server.listen(process.env.APP_PORT, () => {
        console.log('Server listened on ' + process.env.APP_PORT)
    })

    return {
        app,
        server
    }
}

module.exports = runServer
