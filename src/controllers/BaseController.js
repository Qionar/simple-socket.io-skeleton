class BaseController {

    constructor() {
        if(this.constructor === BaseController) {
            throw new Error("BaseController: abstract class can't be instantiated.")
        }
    }

}

module.exports = BaseController