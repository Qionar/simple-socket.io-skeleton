const router = require('express').Router()
const ApiController = require('../controllers/ApiController')

router.get('/test', ApiController.test)

module.exports = router
