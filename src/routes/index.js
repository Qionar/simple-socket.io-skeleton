const apiRouter = require('./api')

const runRoutes = (app) => {
    app.use(apiRouter)
}

module.exports = runRoutes