# Simple Websockets Skeleton

## Installation

```
    npm install
    
    ln -f -s .env.example .env
    
    npm run dev
```

## Overview

#### Controllers

`src/controllers` - controllers

#### Routing
`src/routes/index.js` - define routes here

Create new route files in `src/routes`, require it in `src/index.js` and add to `runRoutes` function
```js
    const apiRouter = require('./api')

    const runRoutes = (app) => {
        app.use(apiRouter)
    }
    
    module.exports = runRoutes
```

#### Services

`src/services` - services

#### Events ( socket.on() )

`src/sockets/events.js` - define your events here, and feel free to pass Service method as a socket callback

```js
    const TestService = require('../services/Test')
    
    
    const runSockets = (socket, io) => {
    
        // sockets events...
    
        socket.on("hello", TestService.hello)
    
    }
    
    module.exports = runSockets
    
    // TestService...
    
    class TestService {

        hello(socket) {
            socket.emit('hello-emit', "Hello, whats up?")
        }
    
    }

    module.exports = new TestService
```
