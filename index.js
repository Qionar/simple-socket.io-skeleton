const runServer = require('./src/server')
const runRoutes = require('./src/routes/index')
const runSockets = require('./src/sockets/events')

runServer(
    (app) => {
        runRoutes(app)
    },
    (socket, io) => {
        runSockets(socket, io)
    }
)